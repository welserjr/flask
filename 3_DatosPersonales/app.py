__author__ = 'WelserJr'

from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/save', methods=['GET', 'POST'])
def save():
    if request.method == 'POST':
        name = request.form['name']
        lastname = request.form['lastname']
        age = request.form['age']
        email = request.form['email']
        data = {'name': name, 'lastname': lastname, 'age': age, 'email': email}
        return render_template('datos_personales.html', **data)
        # return "I am {} {}, I am {} years old and my email is {}".format(name, lastname, age, email)

    else:
        return render_template('index.html')

app.run(debug=True, host='localhost', port=2020)