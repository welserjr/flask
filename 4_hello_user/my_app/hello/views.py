__author__ = 'WelserJr'

from flask import Blueprint, render_template, request

hello = Blueprint('hello', __name__)

@hello.route('/')
@hello.route('/hello')
def hello_user():
    user = request.args.get('user', 'Welser')
    return render_template('index.html', user=user)